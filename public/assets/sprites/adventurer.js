const adventurerCfg = {
  preload: function({that,name='adventurer'}) {
    console.log('Loading Adventurer')
    that.load.atlas(name, 'assets/sprites/adventurer.png', 'assets/sprites/adventurer.json');
  },
  create: function({that,name='adventurer'}) {
    console.log('Setting Adenvturer Animations');
    that.anims.create({ key: 'idle', frames: that.anims.generateFrameNames(name, { prefix: 'idle-', end: 4, zeroPad: 0 }), repeat: -1, frameRate: 8, repeatDelay:100 });
    that.anims.create({ key: 'crouch', frames: that.anims.generateFrameNames(name, { prefix: 'crouch-', end: 4, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'run', frames: that.anims.generateFrameNames(name, { prefix: 'run-', end: 6, zeroPad: 0 }), repeat: -1, frameRate: 8 });
    that.anims.create({ key: 'jump', frames: that.anims.generateFrameNames(name, { prefix: 'jump-', end: 8, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'fall', frames: that.anims.generateFrameNames(name, { prefix: 'fall-', end: 2, zeroPad: 0 }), repeat: -1, frameRate: 8 });
    that.anims.create({ key: 'slide', frames: that.anims.generateFrameNames(name, { prefix: 'slide-', end: 2, zeroPad: 0 }), repeat: -1, frameRate: 8 });
    that.anims.create({ key: 'stand', frames: that.anims.generateFrameNames(name, { prefix: 'stand-', end: 3, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'hang', frames: that.anims.generateFrameNames(name, { prefix: 'hang-', end: 4, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'climb', frames: that.anims.generateFrameNames(name, { prefix: 'climb-', end: 5, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'idle-armed', frames: that.anims.generateFrameNames(name, { prefix: 'idle-armed-', end: 4, zeroPad: 0 }), repeat: -1, frameRate: 8 });
    that.anims.create({ key: 'attack-1', frames: that.anims.generateFrameNames(name, { prefix: 'attack-1-', end: 5, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'attack-2', frames: that.anims.generateFrameNames(name, { prefix: 'attack-2-', end: 6, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'attack-3', frames: that.anims.generateFrameNames(name, { prefix: 'attack-3-', end: 6, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'hurt', frames: that.anims.generateFrameNames(name, { prefix: 'hurt-', end: 6, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'die', frames: that.anims.generateFrameNames(name, { prefix: 'die-', end: 4, zeroPad: 0 }), repeat: false, frameRate: 8 });
    that.anims.create({ key: 'leap', frames: that.anims.generateFrameNames(name, { prefix: 'leap-', end: 3, zeroPad: 0 }), repeat: -1, frameRate: 8 });

  },
};